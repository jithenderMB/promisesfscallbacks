const fs = require('fs');

const path = require('path');

let fsOperations = () => {
    return {
        readTheFile: (path, cb) => {
            fs.readFile(path, 'utf-8', (err, data) => {
                if (err) {
                    console.log(err);
                }
                else {
                    cb(data);
                }
            })
        },

        writeInTheFile: (path, data) => {
            fs.writeFile(path, data, { encoding: 'utf-8', flag: 'a' }, (err) => {
                if (err) {
                    console.log(err);
                }
            })
        },

        removeTheFile: (path) => {
            fs.unlink(path, (err) => {
                if (err) {
                    console.log(err);
                }
            })
        }
    }
};

let { readTheFile, writeInTheFile, removeTheFile } = fsOperations();

const readTheUserData = (path) => {

    return new Promise((res, rej) => {

        if (path) {
            readTheFile(path, res);
        }
        else {
            rej('Something went wrong in function readTheUserData')
        }

    })
};

const captalizeDataAndStoreFileName = (path, data, filePath, subFileName) => {

    return new Promise((res, rej) => {

        writeInTheFile(path, data.toUpperCase());
        writeInTheFile(filePath, subFileName);
        res(path);
    })
};

const convertToLowerCaseAndSort = (path, subFilePath, filePath, subFileName) => {

    return new Promise((res, rej) => {

        readTheFile(path, (data) => {

            let lowerCaseAndSortData = data.toLowerCase().split('. ');
            writeInTheFile(subFilePath, JSON.stringify(lowerCaseAndSortData));
            writeInTheFile(filePath, subFileName);
            res(subFilePath);
        })
    })

};

const sortDataAndAppendToFile = (path, subFilePath, filePath, subFileName) => {

    return new Promise((res, rej) => {

        readTheFile(path, (data) => {
            let sortedData = JSON.parse(data).sort((a, b) => {
                return a.length - b.length;
            });
            writeInTheFile(subFilePath, JSON.stringify(sortedData));
            writeInTheFile(filePath, subFileName);
            res(filePath);
        })

    })


}

const deleteFilesByFileName = (path1) => {

    return new Promise((res, rej) => {
        readTheFile(path1, (data) => {
            let x = data.split('\n');
            for (let i of x) {
                removeTheFile(path.resolve(__dirname, `${i}`));
            }
        })
    })

};

module.exports = {
    readTheUserData,
    captalizeDataAndStoreFileName,
    convertToLowerCaseAndSort,
    sortDataAndAppendToFile,
    deleteFilesByFileName
}