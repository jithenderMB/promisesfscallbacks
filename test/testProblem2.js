const path = require('path');

const userDataPath = path.resolve(__dirname, '../lipsum.txt');

const captalizeFileName = 'captalizedData.txt';
const captalizeDataPath = path.resolve(__dirname, '../captalizedData.txt');

const lowerCaseAndSplitFileName = 'lowerCaseAndSplitData.txt';
const lowerCaseAndSplitDataPath = path.resolve(__dirname, '../lowerCaseAndSplitData.txt');

const sortAndAppendFileName = 'sortAndAppendData.txt';
const sortAndAppendDataPath = path.resolve(__dirname, '../sortAndAppendData.txt');

const mainFolderFilePath = path.resolve(__dirname, '../filenames.txt');

const { readTheUserData, captalizeDataAndStoreFileName, convertToLowerCaseAndSort, sortDataAndAppendToFile, deleteFilesByFileName } = require('../problem2');

readTheUserData(userDataPath)
    .then((lipsumData) => {
        return captalizeDataAndStoreFileName(captalizeDataPath, lipsumData, mainFolderFilePath, captalizeFileName + '\n');
    })
    .then((path) => {
        return convertToLowerCaseAndSort(path, lowerCaseAndSplitDataPath, mainFolderFilePath, lowerCaseAndSplitFileName + '\n');
    })
    .then((path) => {
        return sortDataAndAppendToFile(path, sortAndAppendDataPath, mainFolderFilePath, sortAndAppendFileName);
    })
    .then((path) => {
        deleteFilesByFileName(path);
    });

Promise.all([readTheUserData, convertToLowerCaseAndSort, sortDataAndAppendToFile, deleteFilesByFileName])
    .then(() => {
        console.log('Task completed')
    })
    .catch(() => {
        console.log('Task incompleted')
    });