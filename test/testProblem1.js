let {createRandomJsonFilesInAFolder , deleteRandomJsFiles} = require('../problem1');


createRandomJsonFilesInAFolder(5,'randomJsFiles')
.then((filesNameArray)=>{
    deleteRandomJsFiles(filesNameArray,'randomJsFiles');
});

