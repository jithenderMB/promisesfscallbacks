const fs = require('fs');
const { resolve } = require('path');
const path = require('path');

const createRandomJsonFilesInAFolder = (numberOfFiles, folderName) => {

    return new Promise((resolve, reject) => {

        let filesNameArray = [];

        fs.mkdir(path.resolve(__dirname, folderName), (err) => {

            if (err) {
                console.log(err)
            }
        });

        for (let i = 1; i <= numberOfFiles; i++) {

            let fileName = `${i}.js`;

            filesNameArray.push(fileName);

            fs.writeFile(path.resolve(__dirname, folderName, fileName), `this is ${fileName} file`, (err) => {

                if (err) {
                    console.log(err)
                }
            })
        }
        if (filesNameArray){

            resolve(filesNameArray);
        }
        else{

            reject(new Error('Something went wrong in function createRandomJsonFilesInAFolder'));
        }
        
    })
};

const deleteFile = (path) => {

    return new Promise((resolve, reject) => {

        setTimeout(() => {
            if (path){
                resolve(fs.unlink(path, (err) => {
                    if (err) {
                        console.log(err);
                    }
                }))
            }
            else{
                reject(new Error('Something went wrong in function deleteFile'));
            }
            
        }, 1000)
    })
}
const deleteRandomJsFiles = (filesArray, folderName) => {
    return filesArray.reduce((acc, cur) => {
        return acc.then(() => {
            return deleteFile(path.resolve(__dirname, folderName, cur));
        })
    }, Promise.resolve())
};

module.exports = {
    createRandomJsonFilesInAFolder,
    deleteRandomJsFiles
}
